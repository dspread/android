## [4.7.1](https://gitlab.com/[secure]/android/compare/v4.7.0...v4.7.1) (2024-09-04)


### Bug Fixes

* update the online request url to dingding ([d1882b7](https://gitlab.com/[secure]/android/commit/d1882b7b7fb821a874e4a732f07d7dc555aa74af))

# [4.7.0](https://gitlab.com/[secure]/android/compare/v4.6.12...v4.7.0) (2024-08-28)


### Bug Fixes

* add the cancelTrade method to the return key on the trading interface ([46c520c](https://gitlab.com/[secure]/android/commit/46c520c866709a7c06e381d63aa6993136196aa1))


### Features

* update the network link address ([9625c53](https://gitlab.com/[secure]/android/commit/9625c535f5d543ab9fd51d07a81c10f89742b595))

## [4.6.12](https://gitlab.com/[secure]/android/compare/v4.6.11...v4.6.12) (2024-08-28)


### Bug Fixes

* synchronized master branch code ([fac3963](https://gitlab.com/[secure]/android/commit/fac3963444207f935eef5bd39dd78e8f319fafcf))
* update the backend network url address ([92ef3c8](https://gitlab.com/[secure]/android/commit/92ef3c84bd7c585bde3a92478faf6cee599d2c46))

## [4.6.7](https://gitlab.com/[secure]/android/compare/v4.6.6...v4.6.7) (2024-04-23)


### Bug Fixes

* a non empty check was made when the transaction returned card data ([9cefa61](https://gitlab.com/[secure]/android/commit/9cefa619dbe921ecf43e05c997a6147ddc3254ea))
* add and handle try another interface error transactions update sdkjar package ([84fc3d6](https://gitlab.com/[secure]/android/commit/84fc3d64b5370ca61863a60dcccbf0f8c48f1aef))
* add automatic trading function and fix bug ([a2f986d](https://gitlab.com/[secure]/android/commit/a2f986db7fe735a80c02edc19e441bda637e653a))
* add continuous transactions and fix some bugs ([af93f72](https://gitlab.com/[secure]/android/commit/af93f72bba7e698aa5443130797b79de8d3f04c6))
* add OnlinePin and offlinePin prompts ([6e3c2f5](https://gitlab.com/[secure]/android/commit/6e3c2f5c929bbc7cd35bec0ce0bd281f89617dc5))
* added processing for reading locked chip cards in applications ([7933fcb](https://gitlab.com/[secure]/android/commit/7933fcb0bf551a2acf95b747ffc2177ce41eb91b))
* display of transaction blocking password input and log modification after clearing ([ba2b165](https://gitlab.com/[secure]/android/commit/ba2b165f99f9b60b8e09e77b57f811a05ec88028))
* fix serial port transaction bugs and optimize transaction processes ([b82c1de](https://gitlab.com/[secure]/android/commit/b82c1de7acec315228b924056a97a3fb8b0cb54b))
* fix the demo build.gradle issue ([6050c13](https://gitlab.com/[secure]/android/commit/6050c133abb2f52682f529780ebc4c609524ea1e))
* modify the posinfo display information and optimize the code ([c3d1b31](https://gitlab.com/[secure]/android/commit/c3d1b31068ae36917893efd3b22d536b02215fce))
* modify the process of obtaining the posid for each transaction to obtain the poid upon opening the serial port, and add the function of clearing logs ([af2a163](https://gitlab.com/[secure]/android/commit/af2a163b78a73f99918e5d3e3a1e8f25316f50e4))
* modifying bugs in continuous transactions ([9483ebe](https://gitlab.com/[secure]/android/commit/9483ebec3ff6043d49b6109fb09b847347a40f0f))
* modifying device power display issues ([3b251e3](https://gitlab.com/[secure]/android/commit/3b251e3f8f1d26d363c95b38f3a71806561742aa))
* optimized the menu button click issue during the trading process and optimized the automatic trading process ([c066a91](https://gitlab.com/[secure]/android/commit/c066a91e4f63e4f7b8e52fcf7b4e8ee84464a3b6))
* update the package name for activating the scanning function ([8aa85bf](https://gitlab.com/[secure]/android/commit/8aa85bff252116cc431e9e905073c46179faae73))

## [4.6.6](https://gitlab.com/[secure]/android/compare/v4.6.5...v4.6.6) (2024-03-04)


### Bug Fixes

* modify the scanning function ([2fae3ac](https://gitlab.com/[secure]/android/commit/2fae3ac4c657f422867265b395473f0bff6e16bf))

## [4.6.5](https://gitlab.com/[secure]/android/compare/v4.6.4...v4.6.5) (2024-03-04)


### Bug Fixes

* add mifare cards function ([807682b](https://gitlab.com/[secure]/android/commit/807682bbcecce7644e03549a841565742c710dc3))
* change password keyboard calss file name ([fd3ddbb](https://gitlab.com/[secure]/android/commit/fd3ddbb10aa98f3df52dd7e47b3e8a3adb44f0fe))
* modify the text to display in chinese ([cb11f19](https://gitlab.com/[secure]/android/commit/cb11f19f879e9e12e90ea3cfa2dc137ac38f4443))
* update pin input logic ([825742e](https://gitlab.com/[secure]/android/commit/825742e23c3ef81d4ca13bf77c8e2dfb801d4511))

## [4.6.2](https://gitlab.com/[secure]/android/compare/v4.6.1...v4.6.2) (2024-01-05)


### Bug Fixes

* add the sendOnlineProcessResult (null) method ([05e48e0](https://gitlab.com/[secure]/android/commit/05e48e06935a6b2f45521cec877c4b02b1580c87))
* delete android-pos-demo-work-doc.o3e5ib2o.sw.md ([f94eeab](https://gitlab.com/[secure]/android/commit/f94eeab00a071b66068723b49c4f34e0a2d7d20d))
* delete calls to cancel transactions and add print exit closure methods ([3903d64](https://gitlab.com/[secure]/android/commit/3903d6473e15517a93ba53dd6deecad8bd98ec44))
* delete swimm.json ([8e5c544](https://gitlab.com/[secure]/android/commit/8e5c544e84143d7b1be6ce5436c90222f3e9c78d))
* merge branch 'master' into develop ([ae9a312](https://gitlab.com/[secure]/android/commit/ae9a31218c69c23c4cc9e2475b4cefb351dc0f35))
* update swimm.json ([e8cd7d9](https://gitlab.com/[secure]/android/commit/e8cd7d9876db968e29bf01192753a375ad49552e))
* updated the fastjson version ([ed041e2](https://gitlab.com/[secure]/android/commit/ed041e2a023707bb5a010ba1340c2004fa0459be))

## [4.5.1](https://gitlab.com/[secure]/android/compare/v4.5.0...v4.5.1) (2023-10-13)


### Bug Fixes

* update the upload logs feature that upload logs to dingding reboot ([a6f9bfa](https://gitlab.com/[secure]/android/commit/a6f9bfa93848995871baab221df54aa697220929))

# [4.5.0](https://gitlab.com/[secure]/android/compare/v4.4.1...v4.5.0) (2023-09-27)


### Features

* add the scan functions and fix some bugs ([608f260](https://gitlab.com/[secure]/android/commit/608f2605f4bc1de1a7e84f3f5af3434d43c6ac93))

## [4.4.1](https://gitlab.com/[secure]/android/compare/v4.4.0...v4.4.1) (2023-09-25)


### Bug Fixes

* add the printer features and improve the code logic which optimized printing class name ([3569956](https://gitlab.com/[secure]/android/commit/3569956d644baa291766d1d683048e83ba21a765))

# [4.4.0](https://gitlab.com/[secure]/android/compare/v4.3.2...v4.4.0) (2023-09-21)


### Features

* add the log storation and uploading to web features ([68df216](https://gitlab.com/[secure]/android/commit/68df2162da6eec864c1e280dc836c516371825d9))

## [4.3.2](https://gitlab.com/[secure]/android/compare/v4.3.1...v4.3.2) (2023-09-21)


### Bug Fixes

* prompt for modifying transaction title ([93072f0](https://gitlab.com/[secure]/android/commit/93072f00fcdf93ce6c9fabba7ed7cba4e715192e))

## [4.3.1](https://gitlab.com/[secure]/android/compare/v4.3.0...v4.3.1) (2023-09-12)


### Bug Fixes

* improve the code logic and delete some unused code ([f6278b8](https://gitlab.com/[secure]/android/commit/f6278b8da47820611d854ff5e94b6e5d8972fddd))
* modify bluetooth search image ([305b4a0](https://gitlab.com/[secure]/android/commit/305b4a03ee681daafc82df536788014f1fd928e7))
* remove ambiguity in serial connection ([ee9326c](https://gitlab.com/[secure]/android/commit/ee9326caa5b98aaab320e236cacf37a381c93353))

# [4.3.0](https://gitlab.com/[secure]/android/compare/v4.2.12...v4.3.0) (2023-08-28)


### Bug Fixes

* implement unified modification of project names with existing project file names ([b93000a](https://gitlab.com/[secure]/android/commit/b93000aaa4e77a181efe733b34606bb1083201b9))
* modify code according to method naming rules ([b1c0a44](https://gitlab.com/[secure]/android/commit/b1c0a444d4a80076bc4763d80152bb516031842c))
* modify files that failed compilation ([f6da7f8](https://gitlab.com/[secure]/android/commit/f6da7f8fc9efe437651b57d3edfd70674f63d532))
* modify the application package name and remove the welcome interface ([ded8caf](https://gitlab.com/[secure]/android/commit/ded8cafb619ec4c22286f3dd3442bbb0ceecbf88))
* modify the cashback amount input method ([546d8e0](https://gitlab.com/[secure]/android/commit/546d8e0581df2b7e2edd0af62d480ea2ac4af3b1))
* modify the class name to optimize the code ([17f2cd7](https://gitlab.com/[secure]/android/commit/17f2cd73c3d1ce1c5c1cdd68d856441f3f8bbf09))
* optimize the code encoding format ([a0d7646](https://gitlab.com/[secure]/android/commit/a0d764668532d869327aee38c2bf9fb671544859))
* problems during the compilation process of modifying strings files ([e727282](https://gitlab.com/[secure]/android/commit/e7272820a82630a35fda67c97779f766c3f81d49))
* removed the jniLibs file to address the issue of failed introduction of so files ([1f3e482](https://gitlab.com/[secure]/android/commit/1f3e482162a2c707af9a3eca1fc7477ecae62155))


### Features

* update the android demo UI layout, and add the payment/POS info/POS update/settings/app update functions ([cdb061d](https://gitlab.com/[secure]/android/commit/cdb061d17446175f42f6b758d7da33392c66fa4d))


# [4.2.13](https://gitlab.com/[secure]/android/compare/v4.2.12...v4.3.0) (2023-08-22)


### Bug Fixes

* implement unified modification of project names with existing project file names ([b93000a](https://gitlab.com/[secure]/android/commit/b93000aaa4e77a181efe733b34606bb1083201b9))
* modify files that failed compilation ([f6da7f8](https://gitlab.com/[secure]/android/commit/f6da7f8fc9efe437651b57d3edfd70674f63d532))
* problems during the compilation process of modifying strings files ([e727282](https://gitlab.com/[secure]/android/commit/e7272820a82630a35fda67c97779f766c3f81d49))
* removed the jniLibs file to address the issue of failed introduction of so files ([1f3e482](https://gitlab.com/[secure]/android/commit/1f3e482162a2c707af9a3eca1fc7477ecae62155))


### Features

* update the latest android demo ([cdb061d](https://gitlab.com/[secure]/android/commit/cdb061d17446175f42f6b758d7da33392c66fa4d))

## [4.2.12](https://gitlab.com/[secure]/android/compare/v4.2.11...v4.2.12) (2023-08-04)


### Bug Fixes

* update the gitlab CI CD file to skpi the second CI push ([c901e01](https://gitlab.com/[secure]/android/commit/c901e014471dd50d04fc0c193853c9e53dd03e3f))

## [4.2.11](https://gitlab.com/[secure]/android/compare/v4.2.10...v4.2.11) (2023-08-04)


### Bug Fixes

* delete unused files ([e2ddd97](https://gitlab.com/[secure]/android/commit/e2ddd9708f8c99291f35e80cd481ffccb29c97a1))

## [4.2.10](https://gitlab.com/[secure]/android/compare/v4.2.9...v4.2.10) (2023-08-01)


### Bug Fixes

* update the gitlab config for CHANGELOG and release note ([d877cf8](https://gitlab.com/[secure]/android/commit/d877cf8e046dc8fad1560f6f76217d3f6a5b077d))
